import time

from redis import Redis
from rq import get_current_job


def slow_task(n):
    job = get_current_job()
    job.meta['progress'] = 0
    job.save_meta()
    for i in range(n):
        print(f"Working... {i}")
        job.meta['progress'] = int(i * 100 / n)
        job.save_meta()
        time.sleep(1)

    print("Task completed!")
    return 'Done!'
