from flask import Flask, render_template
from redis import Redis
from rq import Queue
from rq.job import Job
import rq_dashboard

from background_worker import slow_task

app = Flask(__name__)

redis = Redis(host='redis', port=6379, db=0)
q = Queue(connection=redis)

app.config['RQ_DASHBOARD_REDIS_URL'] = "redis://redis:6379"
# setup rq dashboard
app.config.from_object(rq_dashboard.default_settings)
rq_dashboard.web.setup_rq_connection(app)
app.register_blueprint(rq_dashboard.blueprint, url_prefix='/dashboard')




@app.route("/")
def hello_world():  # put application"s code here
    return render_template("main.html")


@app.route("/trigger", methods=["POST"])
def trigger():
    job = q.enqueue(slow_task, 3)

    return render_template("progress_snippet.html", job_key=job.id, polling=True)


@app.route("/progress_report/<job_key>", methods=["GET"])
def progress_report(job_key):
    job = Job.fetch(job_key, connection=redis)

    if job.is_finished:
        return render_template("progress_snippet.html", progress=100, polling=False, job_key=job_key)
    if job.get_status() == "failed":
        return "Job failed", 400
    else:
        progress = job.meta.get('progress', 0)
        print(progress)
        if progress:
            return render_template("progress_snippet.html", progress=int(progress), polling=True, job_key=job_key)
        else:
            return render_template("progress_snippet.html", progress=100, polling=False, job_key=job_key, fail=True)